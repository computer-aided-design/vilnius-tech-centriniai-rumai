# Vilnius TECH Centriniai Rumai Building 3D Model Project

Project Presentation Link (on Microsoft Sway): https://sway.cloud.microsoft/NebOKClF2ySp7eZp?ref=Link

This AutoCAD 2023 project is part of an art course and represents a 3D model of my home university's central building. The central building is known for its unique and aesthetically pleasing design, making it an excellent subject for a creative project. Given that no blueprint was provided, I undertook the task of creating the 3D model based on real measurements, calculated using geometrical rules. The ultimate goal of this project is to successfully 3D print a detailed replica of the central building.

## Project Overview

**Project Title:** University Central Building 3D Model

**Software Used:** AutoCAD 2023

**Objective:** To create a detailed and accurate 3D model of the central building of my home university for 3D printing.

## Key Steps and Details

1. **Research and Observation**: The project began with a thorough observation of the central building from different angles. I studied its architectural details, including the facades, windows, doors, and decorative elements.

2. **Measurements**: As no blueprint was available, I measured the building manually. Geometrical rules and techniques were employed to ensure accurate measurements, ensuring that the 3D model would be as close to reality as possible.

3. **AutoCAD Modeling**: Using AutoCAD 2023, I created a 3D model of the central building. This involved modeling the building's exterior, including walls, windows, and roof, to replicate the unique design.

4. **Texture and Materials**: To add realism to the model, I applied textures and materials that match the actual building's appearance. This involved selecting appropriate colors, materials, and finishes for different parts of the structure.

5. **3D Printing Preparation**: Ensuring the 3D model is suitable for 3D printing was a critical step. I optimized the model for 3D printing by making sure it was in the right format and that there were no issues with wall thickness or other potential printing problems.

6. **Quality Control**: A thorough quality control check was conducted to make sure the 3D model was error-free and would translate well into a physical replica.

7. **3D Printing**: The final step involves sending the 3D model to a 3D printer to bring it to life. I use high-quality printing materials to ensure the details and aesthetics of the central building are faithfully recreated.

## Project Outcomes

Upon successful completion of this AutoCAD 2023 project, I will have a 3D printed replica of my university's central building. This tangible representation of my university's architectural masterpiece can serve as a unique art piece or a memento of my educational journey.

This project not only demonstrates my artistic desire and technical skills but also my determination to bring a creative vision to life, even without readily available blueprints. It's a testament to my commitment to preserving the beauty of my university's central building through the art of 3D modeling and printing.
